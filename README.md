# Super Blog

Aplicação Ruby On Rails para criação, curadoria e leitura de artigos.

----------

Este projeto usa Rubocop para análise e formatação de código, sempre que alterá-lo rode:
```sh
$ rubocop
```

Este projeto tem testes automatizados, para executá-los use:
```sh
$ rspec .
```

Para executar a aplicação em ambiente de desenvolvimento use:
```sh
$ docker compose up
```

----------

Atualmente a aplicação está disponível em http://35.198.32.87/articles
