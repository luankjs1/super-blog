# frozen_string_literal: true

FactoryBot.define do
  factory :tag do
    name { Faker::Color.color_name }
  end
end
